This is a demo application for Spring 4
The Agenda is the following

INTRODUCTION TO SPRING

● Java configuration and the Spring application
context

● @ Configuration and @  Bean annotations

● @ Import: working with multiple configuration files

● Defining bean scopes

● Launching a Spring Application and obtaining
Beans

SPRING JAVA CONFIGURATION: A DEEPER LOOK

● External properties & Property sources

● Environment abstraction

● Using bean profiles

● Spring Expression Language (SpEL)

● How it Works: Inheritance based proxies

ANNOTATION-BASED DEPENDENCY INJECTION

● Autowiring and component scanning

● Java configuration versus annotations, mixing.

● Lifecycle annotations: @ PostConstruct and
@ PreDestroy

● Stereotypes and meta-annotations
FACTORY PATTERN IN SPRING

● Using Spring FactoryBeans
ADVANCED SPRING: HOW DOES SPRING WORK
INTERNALLY?

● The Spring Bean Lifecycle

● The BeanFactoryPostProcessor interception point

● The BeanPostProcessor interception point

● Spring Bean Proxies

● @ Bean method return types

TESTING A SPRING-BASED APPLICATION

● Spring and Test Driven Development

● Brief overview of JUnit 5

● Spring 5 integration testing with JUnit 5

● Application context caching and the
@ DirtiesContext annotation

● Profile selection with @ ActiveProfiles

● Easy test data setup with @ Sql

ASPECT-ORIENTED PROGRAMMING

● What problems does AOP solve?

● Differences between Spring AOP and AspectJ

● Defining pointcut expressions

● Implementing an advice: @ Around, @ Before,
@ After

DATA ACCESS AND JDBC WITH SPRING

● How Spring integrates with existing data access
technologies

● DataAccessException hierarchy

● Implementing caching using @ Cacheable •
Embedded databases for testing

● Spring‘s JdbcTemplate
DATABASE TRANSACTIONS WITH SPRING

● Transactions overview

● Transaction management with Spring

● Isolation levels, transaction propagation and
rollback rules

● Transactions and integration testing

JPA WITH SPRING AND SPRING DATA

● Quick introduction to ORM with JPA

● Benefits of using Spring with JPA

● JPA configuration in Spring

● SPRING BOOT

● Using Spring Boot to bypass most configuration

● Simplified dependency management with starter
POMs

● Easily overriding Spring Boot defaults

ADVANCED SPRING JPA

● Configuring Spring JPA using Spring Boot

● Spring Data JPA dynamic repositories

SPRING IN A WEB APPLICATION

● Configuring Spring in a Web application

● Introduction to Spring MVC, required configuration

● Controller method signatures

● Views and ViewResolvers

● Using @ Controller and @ RequestMapping
annotations

● Configuring Spring MVC with Spring Boot

● Spring Boot packaging options, JAR or WAR

SPRING BOOT-GOING FURTHER (OPTIONAL)

● Going beyond the default settings

● Customizing Spring Boot configuration

● Logging control

● Configuration properties using YAML

● Boot-driven testing

SPRING SECURITY

● What problems does Spring Security solve?

● Configuring authentication and intercepting URLs

● Spring Security support for server-side rendering

● Security at the method level

● Understanding the Spring Security filter chain

REST WITH SPRING MVC

● An introduction to the REST architectural style

● Controlling HTTP response codes with
@ ResponseStatus

● Implementing REST with Spring MVC,
@ RequestBody, @  ResponseBody

● Spring MVC’s HttpMessageConverters and
automatic content negotiation
