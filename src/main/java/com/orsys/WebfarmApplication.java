package com.orsys;

import com.orsys.model.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication

@ComponentScan(basePackages = {"com.ge","com.orsys"})
public class WebfarmApplication {



    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(WebfarmApplication.class, args);
        System.out.println(  context.getBean("animal",FarmAnimal.class).sound());
//        System.out.println(   context.getBean(FarmAnimal.class).sound());
//        System.out.println(   context.getBean(FarmAnimal.class).sound());
//        System.out.println(    context.getBean(FarmAnimal.class).sound());

    }


}
