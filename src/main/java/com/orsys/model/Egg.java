package com.orsys.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class Egg {
    private FarmAnimal chicken;

    @Autowired
    public Egg( FarmAnimal chicken) {
        this.chicken = chicken;
    }
}
