package com.orsys.model;

public class Barn {
    private final String usage;

    public Barn(String usage) {
        this.usage = usage;
    }

    public String getUsage() {
        return usage;
    }
}
