package com.orsys.model;

public class Pig implements FarmAnimal {
    @Override
    public String sound() {
        return "GRrrr";
    }
}
