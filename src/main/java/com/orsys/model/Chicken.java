package com.orsys.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
public class Chicken implements FarmAnimal {

  //  private Egg egg;

    //@Autowired
    public Chicken() {
    //    System.out.println(Arrays.asList(egg.getClass().getMethods()).stream().map((s)->s+"\n").collect(Collectors.toList()));
        //this.egg = egg;
    }

    @Override
    public String sound() {
        return "cluck";
    }


}
