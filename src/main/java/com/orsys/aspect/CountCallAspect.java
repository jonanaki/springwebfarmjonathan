package com.orsys.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
public class CountCallAspect {

    Map<Class<?>,Integer> cache = new HashMap();
 //   @Before("@annotation(com.orsys.aspect.Countable)")
    @Before("execution(* sound())")
    public void count(JoinPoint joinPoint){
        System.out.println(joinPoint);
        System.out.println( joinPoint.getKind());
        System.out.println( joinPoint.getThis());
        Class<?> aClass = joinPoint.getThis().getClass();
        Integer integer = cache.get(aClass);
        if(integer == null){
            integer = 0;

        }
        cache.put(aClass , ++integer);
        System.out.println(aClass+" make sound "+integer+ " times");

    }

    @Around("execution(* *..FarmAnimal.sound())")
    public Object count(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        System.out.println(System.currentTimeMillis() - start);
       return proceed;


    }

}
