package com.orsys.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
public class Const {
    @Value("${config.name}")
    String test;

    @Bean
    public String test(){
        return test;
    }

    @Bean
    public String test2(){
        return test;
    }

}
