package com.orsys.config;

import com.orsys.model.Chicken;
import com.orsys.model.FarmAnimal;
import com.orsys.model.Pig;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Configuration
public class Animals {

    @Bean

    FarmAnimal animal(){
       return  new Chicken() ;
    }
}
